# List of TODOs


## Features
List of features to add.
~~- Add an option in components to change how it is displayed (For ex. rounded-edges for border-radius).~~
- Both `ui-group` and `ui-item` are meant to use for lists of data. And `ui-form/ui-group` is meant for grouping `ui-label` and `ui-input`. This behavior should be adjusted to have only one `ui-group`, whether by changing its name or extending the `ui-group` functionality.
- Add `afterInstall()` method to install Font Awesome and Bootstrap packages.
- Add optional organization of app styles.

## Fixes
List of TODO fixes.
- Allow importing `Bootstrap` styles once [only on addon].
- Allow addon importing to hosting apps styles.
