import Component from '@ember/component';
import layout from '../../templates/components/layout/ui-footer';

export default Component.extend({
  layout,

  tagName: 'footer',

  classNames: ['ui-footer']
});
