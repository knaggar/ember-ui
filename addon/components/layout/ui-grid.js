import Component from '@ember/component';
import { computed } from '@ember/object';
import { typeOf } from '@ember/utils';
import layout from '../../templates/components/layout/ui-grid';

export default Component.extend({
  layout,

  classNames: ['col'],

  classNameBindings: ['gridLayout', 'centerGrid:mx-auto'],

  centerGrid: false,

  /**
   * @property grid
   * @type {String}
   * @default null
   * @description Property to change the breakpoints of bootstrap grid system.
   * @method gridLayout
   */
  grid: null,
  gridLayout: computed('grid.[]', function() {
    if (typeOf(this.grid) == 'array') {
      return this.grid.map(col => `col-${col}`).join(' ')
    }

    if (typeOf(this.grid) == 'string' || typeOf(this.grid) == 'number') {
      return `col-${this.grid}`
    }
  })
});
