import Component from '@ember/component';
import layout from '../../templates/components/layout/ui-header';

export default Component.extend({
  layout,

  tagName: 'header',

  classNames: ['ui-header', 'navbar']
});
