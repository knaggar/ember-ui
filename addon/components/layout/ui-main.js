import Component from '@ember/component';
import layout from '../../templates/components/layout/ui-main';

export default Component.extend({
  layout,

  tagName: 'main',

  classNames: 'ui-main'
});
