import Component from '@ember/component';
import layout from '../templates/components/ui-button';
import { computed } from '@ember/object';
import uiStyle from '../mixins/ui-style';
import uiShape from '../mixins/ui-shape';


export default Component.extend(uiStyle, uiShape, {
  layout,

  tagName: "button",

  uiElement: 'btn',

  classNames: ['ui-button'],

  classNameBindings: ['directionStyle', 'sizeStyle'],

  attributeBindings: ['type', 'disable'],

  // color: 'light',

  /**
   * @property direction
   * @type {String}
   * @default null
   * @description define shape for button. Available values: 'right', 'left'.
   * @extends directionStyle
   */
  direction: null,
  directionStyle: computed('direction', function(){
    if (this.get('direction')) {
      return `float-${this.get('direction')}`
    }
  }),

  /**
   * @property size
   * @type {String}
   * @default null
   * @description define sizes for button. Available values: 'block', 'lg' & 'sm'.
   * @extends directionStyle
   */
  size: null,
  sizeStyle: computed('size', function(){
    if (this.get('size')) {
      return `${this.get('uiElement')}-${this.get('size')}`
    }
  }),

  iconSize: null,

  noBackground: false,

  didReceiveAttrs() {
    this._super(...arguments);
    // TODO replace with an additional style
    // IDEA style will color the content of button and removes background
    if (this.noBackground) {
      this.set('uiStyle', `btn text-${this.color}`)
    }
  },


  // classNameBindings: ['buttonContent', 'activeButton'],
  // attributeBindings: ['toggle:data-toggle', 'poppup:aria-haspopup', 'expanded:aria-expanded', 'placement:data-placement', 'title'],
  //
  // hasAction: true,
  // toggle: null,
  // poppup: null,
  // expanded: null,
  // placement: computed('toggle', function(){
  //   if (this.get('toggle') == 'tooltip')
  //     return 'top'
  // }),
  // title: null,
  // activeButton: false,
});
