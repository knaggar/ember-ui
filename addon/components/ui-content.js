import Component from '@ember/component';
import layout from '../templates/components/ui-content';

export default Component.extend({
  layout,

  classNames: ['ui-content']
});
