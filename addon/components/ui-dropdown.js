import Component from '@ember/component';
import layout from '../templates/components/ui-dropdown';

export default Component.extend({
  layout,

  classNames: ['ui-dropdown', 'dropdown']

});
