import Component from '@ember/component';
import layout from '../templates/components/ui-form-group';
import { computed } from '@ember/object';

export default Component.extend({
  layout,

  classNameBindings: ['formGroup'],

  formGroup: true,

  childControl: 'input',

  getFormControl: computed('childControl', {
    get() {
      return `ui-${this.get('childControl')}`
    }
  }),

  // actions: {
  //   togglePasswordVisibility(){
  //     console.log('Clicked!')
  //     this.set('type', 'text')
  //     // TODO PreverntDefault()
  //   }
  // }



  //
  // didInsertElement() {
  //   this._super(...arguments)
  //
  //   // Set label `for` attribute from input id
  //   let inputID = this.$('input').attr('id')
  //   this.set('inputId', inputID)
  // }
});
