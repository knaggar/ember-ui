import Component from '@ember/component';
import layout from '../templates/components/ui-form';

export default Component.extend({
  layout,

  tagName: 'form',

  classNames: ['ui-form'],

  // init() {
  //   this._super(...arguments)
  //   // console.info('childViews', this.get('childViews'))
  // },

  formSubmit() {},

  submit() {
    this.formSubmit()

    return false
  }


});
