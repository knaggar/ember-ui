import Component from '@ember/component';
import layout from '../templates/components/ui-group';
import { computed } from '@ember/object';

export default Component.extend({
  layout,

  classNames: ['ui-group'],

  classNameBindings: ['container'],

  childSeries: null,

  groupFor: computed('childSeries', function() {
    let children = this.get('childSeries')

    if (children == 'card') {
      return 'deck'
    } else {
      return 'group'
    }
  }),
  container: computed('childSeries', 'groupFor', function() {
    return `${this.get('childSeries')}-${this.get('groupFor')}`
  })
});
