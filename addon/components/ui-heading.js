import Component from '@ember/component';
import layout from '../templates/components/ui-heading';
import { computed } from '@ember/object';

export default Component.extend({
  layout,

  classNames: ['ui-heading'],

  /**
   * @property direction
   * @type {String}
   * @default null
   * @description define shape for button. Available values: 'right', 'left'.
   * @extends directionStyle
   */
  align: 'left',
  alignStyle: computed('align', function(){
    if (this.get('align')) {
      return `text-${this.get('align')}`
    }
  }),

  classNameBindings: ['alignStyle'],
});
