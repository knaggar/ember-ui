import TextField from '@ember/component/text-field';
import layout from '../templates/components/ui-input';
import { computed } from '@ember/object';
import uiShape from '../mixins/ui-shape';

export default TextField.extend(uiShape, {
  layout,

  uiElement: 'input',

  classNames: ['ui-input', 'form-control'],

  placeholder: null,

  autocomplete: computed('placeholder', function() {
    return this.get('placeholder')
  }),

  attributeBindings: ['type', 'placeholder', 'autocomplete'],
});
