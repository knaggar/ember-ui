import Component from '@ember/component';
import layout from '../templates/components/ui-item';

export default Component.extend({
  layout
});
