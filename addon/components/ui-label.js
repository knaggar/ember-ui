import Component from '@ember/component';
import layout from '../templates/components/ui-label';

export default Component.extend({
  layout,

  tagName: 'label',

  classNames: ['ui-label'],

  attributeBindings: ['for']
});
