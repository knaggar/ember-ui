import LinkComponent from '@ember/routing/link-component';
import layout from '../templates/components/ui-link';
import uiShape from '../mixins/ui-shape';
import uiStyle from '../mixins/ui-style';

export default LinkComponent.extend(uiShape, uiStyle, {
  layout,

  classNames: ['nav-link'],
  // TODO Deattach title if null present
  // IDEA replace with icon
  //
  uiElement: 'text'
});
