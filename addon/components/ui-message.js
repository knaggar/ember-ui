import Component from '@ember/component';
import layout from '../templates/components/ui-message';
import uiStyle from '../mixins/ui-style';

export default Component.extend(uiStyle, {
  layout,

  uiElement: 'alert',

  classNames: ['ui-message']
});
