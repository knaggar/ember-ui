import Component from '@ember/component';
import layout from '../templates/components/ui-nav';
import { computed } from '@ember/object';

export default Component.extend({
  layout,

  classNames: ['ui-nav', 'nav'],

  /**
   * @property direction
   * @type {String}
   * @default horizontal
   * @description Change direction of navigation items.
   *              Avialable values: 'horizontal', 'vertical'
   * @method directionStyle
   */
  direction: 'horizontal',
  directionStyle: computed('direction', function(){
    if (this.direction == 'vertical') {
      return 'flex-column'
    }
  }),

  /**
  * @property align
  * @type {String}
  * @default start
  * @description Change alignment of navigation items.
  *              Avialable values: 'start', 'center', 'end'
  * @method itemsAlignment
  */
  align: null,
  itemsAlignment: computed('direction', 'align', function(){
    if (this.direction == 'horizontal' && this.align != null) {
      return `justify-content-${this.align}`
    }
    if (this.direction  == 'vertical' && this.align != null) {
      return `align-items-${this.align}`
    }
  }),


  classNameBindings: ['directionStyle', 'itemsAlignment'],

  itemsTagName: 'link',
  /**
   * [getNavItems description]
   * @type {[type]}
   */
  getNavItems: computed('itemsTagName', {
    get() {
      if (this.itemsTagName) {
        return `ui-${this.itemsTagName}`
      } else {
        throw new Error(`itemsTagName is ${this.itemsTagName}, please define it.`)
      }
    }
  })
});
