import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';

export default Mixin.create({
    /**
   * @property shape
   * @type {String}
   * @default null
   * @description define shape for button. Available values: 'capsule', 'circle'.
   * @extends shapeStyle
   */
  shape: 'capsule', // TODO set to null and override on addon options,
  shapeStyle: computed('shape', 'uiElement', function(){
    if(this.get('shape')) {
      return `${this.get('uiElement')}-${this.get('shape')}`
    }
  }),

  classNameBindings: ['shapeStyle'],
});
