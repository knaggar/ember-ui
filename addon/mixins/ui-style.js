import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';

export default Mixin.create({
  classNameBindings: ['uiStyle'],
  attributeBindings: ['role'],

  /**
   * @property uiElement
   * @type {String}
   * @default null
   * @description define tag attribute. Not similar to component's `tagName` property
   */
  uiElement: null,

  /**
   * @property color
   * @type {String}
   * @default null
   * @description define the component style used in `bootstrap`.
   */
  color: null,

  /**
   * @property style
   * @type {String}
   * @default null
   * @yields [null, 'outline', 'capsule', 'outline-capsule']
   * @description define the component style used in `bootstrap`.
   */
  style: null,

  /**
   * @property role
   * @type {String}
   * @default null
   * @description define role for `HTML` role attribute.
   */
  role: null,


  /**
   * @method computed
   * @param {String} uiElement
   * @param {String} color
   * @param {String} style
   * @param {Function} function
   * @return return component style defined by `bootstrap` and customized on ember-ui.
   */
  uiStyle: computed('uiElement', 'color', 'style', function() {
    let uiElement = this.get('uiElement')
    let color = this.get('color')
    let style = this.get('style')

    let uiElementOut = style ? `${uiElement}-${style}-${color}` : `${uiElement}-${color}`

    return `${uiElement} ${uiElementOut}`
  })
});
