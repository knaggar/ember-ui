'use strict';

module.exports = {
  description: 'List of configuration for ember-ui',

  // locals(options) {
  //   // Return custom template variables here.
  //   return {
  //     foo: options.entity.options.foo
  //   };
  // }

  afterInstall(/* options */) {
    this.addIconDependencies()
  },

  addIconDependencies() {
    let iconPack

    this.addAddonToProject('@fortawesome/ember-fontawesome')

    if (iconPack == 'regular') {
      this.addPackageToProject('@fortawesome/free-regular-svg-icons')
    }

    if (iconPack == 'solid') {
      this.addPackageToProject('@fortawesome/free-solid-svg-icons')
    }

    if (iconPack == 'all') {
      this.addPackageToProject('@fortawesome/free-regular-svg-icons')
      this.addPackageToProject('@fortawesome/free-solid-svg-icons')
    }
  },
};
