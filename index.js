'use strict';
const mergeTrees = require('broccoli-merge-trees');
const Funnel = require('broccoli-funnel');
const path = require('path');

module.exports = {
  name: require('./package').name,

  included( /* app */ ) {
    this._super.included.apply(this, arguments)
  },

  isDevelopingAddon() {
    return true;
  },

  // treeForStyles() {
  //   return new Funnel(this.getEmberUiStylesPath(), {
  //     destDir: 'ember-ui'
  //   })
  // },

  treeForAddonStyles(tree) {
    let bootstrapTree = new Funnel(this.getBootstrapStylesPath(), {
      destDir: 'bootstrap'
    });

    return mergeTrees([bootstrapTree, tree]);
  },

  // treeForPublic() {
  //   return new Funnel(path.join(this.root, 'public'))
  // },
  //
  // getEmberUiStylesPath() {
  //   let pkgPath = path.dirname(__filename);
  //
  //   return path.join(pkgPath, 'addon', 'styles');
  // },
  //
  getBootstrapStylesPath() {
    let pkgPath = path.dirname(require.resolve(`bootstrap/package.json`))

    return path.join(pkgPath, 'scss')
  }
};
