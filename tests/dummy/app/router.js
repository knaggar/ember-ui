import AddonDocsRouter, { docsRoute } from 'ember-cli-addon-docs/router';
import config from './config/environment';

const Router = AddonDocsRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  docsRoute(this, function() {

  })

  this.route('not-found', {
    path: '/*path'
  });
  this.route('docs', function() {
    this.route('usage');

    this.route('components', function() {
      this.route('ui-button');
      this.route('ui-form');
      this.route('ui-nav');
      this.route('ui-link');
      this.route('ui-message');
      this.route('ui-heading');
    });
  });
});

export default Router;
