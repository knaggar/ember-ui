import EmberObject from '@ember/object';
import UiShapeMixin from 'ember-ui/mixins/ui-shape';
import { module, test } from 'qunit';

module('Unit | Mixin | ui-shape', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let UiShapeObject = EmberObject.extend(UiShapeMixin);
    let subject = UiShapeObject.create();
    assert.ok(subject);
  });
});
