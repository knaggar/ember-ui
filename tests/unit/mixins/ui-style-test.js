import EmberObject from '@ember/object';
import UiStyleMixin from 'ember-ui/mixins/ui-style';
import { module, test } from 'qunit';

module('Unit | Mixin | ui-style', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let UiStyleObject = EmberObject.extend(UiStyleMixin);
    let subject = UiStyleObject.create();
    assert.ok(subject);
  });
});
